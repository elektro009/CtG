/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package BuS;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

/**
 *
 * @author ljubomir
 */
public class Foto {
    BufferedImage Orig;
    BufferedImage Pret;
    String putanja;
    public Foto()
    {
    }
    //Spremiti sliku na disk
    public boolean spremi()
    {
        JFileChooser fileChooser = new JFileChooser();
        if (fileChooser.showSaveDialog(fileChooser) == JFileChooser.APPROVE_OPTION) {
    }
        File putanja_f = fileChooser.getSelectedFile();
        try {
            ImageIO.write(Pret, "JPEG", putanja_f);
        } catch (IOException ex) {
            Logger.getLogger(Foto.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    // Dobivanje putanje za sliku preko dijaloskog okvira
    
    public boolean ucitaj()
    {
        JFileChooser dok = new JFileChooser();
        dok.showOpenDialog(dok);
        putanja=dok.getSelectedFile().toString();
        return ucitaj_orig(putanja);
    }
    //Ucitavanje slike s diska
    private boolean ucitaj_orig(String izvor)
    {
        try {
            Orig=ImageIO.read(new File(izvor));
        } catch (IOException ex) {
            Logger.getLogger(Foto.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
    //Pretvorba slike u boji u crno-bijelu sliku
    public boolean CrnoBijela()
    {
        try {
            Pret=ImageIO.read(new File(putanja));
        } catch (IOException ex) {
            Logger.getLogger(Foto.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(Pret==null)return false;
        for (int x = 0; x < Pret.getWidth(); x++)
        {
            for (int y = 0; y < Pret.getHeight(); y++)
            {
                int rgb = Pret.getRGB(x, y);
                int r = (rgb >> 16) & 0xFF;
                int g = (rgb >> 8) & 0xFF;
                int b = (rgb & 0xFF);

                int cbvrijednost = (r + g + b) / 3;
                
                if(cbvrijednost<128)cbvrijednost=0;
                else cbvrijednost=255;
                
                int cb = (cbvrijednost << 16) + (cbvrijednost << 8)+ cbvrijednost; 
                Pret.setRGB(x, y, cb);
            }
        }
        return true;
    }
    //Pretvorba slike u boji u sivu sliku
    public boolean Siva()
    {
        try {
            Pret=ImageIO.read(new File(putanja));
        } catch (IOException ex) {
            Logger.getLogger(Foto.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        if(Pret==null)return false;
        for (int x = 0; x < Pret.getWidth(); x++)
        {
            for (int y = 0; y < Pret.getHeight(); y++)
            {
                int rgb = Pret.getRGB(x, y);
                int r = (rgb >> 16) & 0xFF;
                int g = (rgb >> 8) & 0xFF;
                int b = (rgb & 0xFF);

                int siva_vrijednost = (r + g + b) / 3;
                
                int siva = (siva_vrijednost << 16) + (siva_vrijednost << 8)+ siva_vrijednost; 
                Pret.setRGB(x, y, siva);
            }
        }
        return true;
    }
    
}
